function math2() {
  var count=0;
  var result=[];
  while(count<500) {
    var a=Math.floor(Math.random() * 19) + 1;
    var b=Math.floor(Math.random() * 19) + 1;
    var op = Math.random();
    var sign;
    var res;
    if (op>0.5) {
      sign="+";
    }
    else {
      sign="-";
      res=a-b;
    }
    
    var temp=a+sign+b+"=";
    if (result.indexOf(temp)===-1&&res>=0){
      result.push(temp);
      count++;
    }
  }
  var str="";
  for (var i=0; i<count; i++) {
    str=str+"<p>"+result[i]+"</p>"
  }
  var main=document.getElementById("main");
  main.innerHTML=str;
}

function math4(){
  var count=0;
  var result=[];
  while(count<500) {
    var a=Math.floor(Math.random() * 31);
    var b=Math.floor(Math.random() * 31);
    var op = Math.random();
    var sign;
    var res;
    if (op>0.5) {
      sign="+";
      res=a+b;
    }
    else {
      sign="-";
      res=a-b;
    }
    var pos = Math.random();
    var temp = "";
    if (pos>0.5)
      temp=a+" "+sign+" _____"+" = "+res;
    else
      temp="_____ "+sign+" "+b+" = "+res;
    
    if (result.indexOf(temp)===-1&&res>0){
      result.push(temp);
      count++;
    }
  }
  var str="";
  for (var i=0; i<count; i++) {
    str=str+"<pre>"+result[i]+"</pre>"
  }
  var main=document.getElementById("main");
  main.innerHTML=str;
}

function math3() {
  var count=0;
  var result=[];
  while(count<1000) {
    var a=Math.floor(Math.random() * 19) + 1;
    var b=Math.floor(Math.random() * 19) + 1;
    var c=Math.floor(Math.random() * 19) + 1;
    var op = Math.random();
    var sign1;
    var sign2;
    var res;
    if (op>0.5) {
      sign1="+";
    }
    else {
      sign1="-";
    }
    op = Math.random();
    if (op>0.5) {
      sign2="+";
    }
    else {
      sign2="-";
    }
    var res=calculate(a,b,c,sign1,sign2);
    var temp=a+sign1+b+sign2+c+"=";
    if (result.indexOf(temp)===-1&&res){
      result.push(temp);
      count++;
    }
  }
  var str="";
  for (var i=0; i<count; i++) {
    str=str+"<p>"+result[i]+"</p>"
  }
  var main=document.getElementById("main");
  main.innerHTML=str;
}

function calculate(a,b,c,s1,s2) {
  var mid;
  if (s1==="-") {
    if (a<b)
      return false;
    else
      mid=a-b;
    if (s2==="-") {
      if ((mid-c)<0)
        return false;
    }
  }
  else {
    mid=a+b;
    if (s2==="-") {
      if (mid<c)
        return false;
    }
  }
  return true;  
}

function multiply(){
  var count=0;
  var result=[];
  while(count<50) {
    var a=Math.floor(Math.random() * 100);
    var b=Math.floor(Math.random() * 10);
    
    var temp=a+" x "+b+"=";
    if (a!=0 && b!= 0){
      if (result.indexOf(temp)===-1){
        result.push(temp);
        count++;
      }
    }
  }
  var str="";
  for (var i=0; i<count; i++) {
    str=str+"<p>"+result[i]+"</p>"
  }
  var main=document.getElementById("main");
  main.innerHTML=str;
}